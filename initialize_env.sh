#!/bin/zsh


# Create a conda virtual environment for the repository
echo 'Creating environment...'

echo "Y" | conda env create -f environment.yml

if [ $? -ne 0 ]; then
    echo "Failed to create environment $output"
else
    mkdir data
    mkdir data/db
    echo 'Success: You have created console-mate-env'
    echo "# To remove this environment, use
    #
    #     $ conda deactivate && conda remove -n console-mate-env --all
    # To start the mongo db server, use 'sudo mongod --dbpath data/db --port 27018'"
fi
