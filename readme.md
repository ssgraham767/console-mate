# This is a basic chatting tool which can be used in the console, which uses OpenAIs chat APIs.

## Setting Up

```
chmod +x initialize_env.sh && ./initialize_env.sh
```

Once you have initialized your environment, you will need to activate it and start your database server.

Activate the environment:

```
conda activate console-mate-env
```

In this same console window, start the mongodb server (this local database stores your conversations):

```
sudo mongod --dbpath data/db --port 27018
```

## Usage

To use the application, open a terminal window in this directory (this repo) and activate the environment:

```
conda activate console-mate-env
```

Now, in the same console window, execute the following...

```
python mate.py
```

*Note: Entering multi-line input can be problematic. To do this more cleanly, use the `file` argument when in a conversation to read a message from `message.txt`*

## Next Steps

- Pass contents files and directories to API
- Allow database interaction (identify tables and dynamically construct queries)


## Screenshot

![An example of the interface](/example.png)