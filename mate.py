import os
import argparse
from classes import Conversation, Message
from utils import define_conversations, rich_print, print_text, print_title

key = os.environ.get('OPEN_AI_KEY')


def initialize_conversation(name, system_prompt):
    """Start a conversation log and place in the database"""
    conversations = define_conversations()
    filter = {'conversation_name': name}
    if len(list(conversations.find(filter))) != 0:
        rich_print('error', 'Please choose a different name')
        raise Exception("Input Error")
    
    conversation = {
        "conversation_name": name,
        "messages": [
            system_prompt.jsonify(),
        ]
    }
    conversations.insert_one(conversation)
    return Conversation(name, conversation['messages'])
    

def begin(conversation_name, model='gpt-4', system_prompt='coder'):
    """Start a conversation"""
    rich_print('system', f'Starting a new conversation with name `{conversation_name}`...\n')
    system_prompts = {'coder':"You are a skilled programmer whose job it is to implement the tasks described"}
    prompt = Message('system', system_prompts[system_prompt])
    conversation = initialize_conversation(conversation_name, prompt)
    conversation.print()
    conversation.proceed(key, model)


def list_conversations():
    """List all conversations"""
    conversations = define_conversations()
    conversations_list = list(conversations.find({}))

    if len(conversations_list) == 0:
        rich_print('system', 'No Conversations Yet...')
    else:
        rich_print('system', f"Hello, you have {len(conversations_list)} conversation{'s' if len(conversations_list) > 1 else ''} saved:")
        for conversation in conversations_list:
            print_text('- ' + conversation['conversation_name'], 'menu', show_role=False)

    return conversations_list


def load_conversation(conversation_name):
    """Locate and print an existing conversation"""
    conversations = define_conversations()
    conversation_name = conversation_name.strip()
    filter = {'conversation_name': conversation_name}
    document = conversations.find_one(filter)
    if not document:
        rich_print('system', 'No conversations with the given name')
        return None
    conversation = Conversation(conversation_name, document['messages'])
    conversation.print()
    return conversation


def handle_initial_user_input(model):
    """Start the program and enter a conversation"""
    conversation_list = list_conversations()
    prompt = f"Please enter the name of a new{' or existing ' if conversation_list else ' '}conversation, or 'exit' to quit"
    rich_print('system', prompt)

    user_input = input("Name: ")
    if user_input == 'exit':
        exit()
    
    # Find conversations with the given name
    conversation = load_conversation(user_input)

    if conversation: # If a conversation is found
        conversation.proceed(key, model)

    else: # If no conversation is found
        begin(user_input, model='gpt-4', system_prompt='coder')
    
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A small application for chatting with LLMs")
    
    parser.add_argument("--model", type=str, help="The model to use for generating a response", default='gpt-4')

    print_title('console mate')
    args = parser.parse_args()
    handle_initial_user_input(args.model)