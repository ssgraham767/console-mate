from rich.console import Console
from rich.text import Text
from rich.syntax import Syntax
from pymongo import MongoClient
import time
from openai import OpenAI

console = Console()
CLIENT_STRING = 'mongodb://localhost:27018/'

def define_conversations():
    # create a connection
    client = MongoClient(CLIENT_STRING)

    # define the database named "conversations_db"
    db = client['conversations_db']

    # define the collection named "conversations"
    conversations = db['conversations']
    return conversations


def delete_conversation(name):
    # create a connection
    client = MongoClient(CLIENT_STRING)
    db = client['conversations_db']
    db.conversations.delete_one({'conversation_name': name})


def rich_print(role, content):
    """Print text, handling formatting of code blocks"""
    for i, item in enumerate(content.split('```')):
        if i%2: # if code block
            print_code_block(item, item.split()[0], theme="monokai")
        else:
            show_role = True if i==0 else False
            print_text(item, role, show_role=show_role)


def print_title(title):
    console.rule(f"[bold blue]{title.upper()}", style="bold blue")


def print_code_block(text, language, theme="monokai"):
    """Print a code block with syntax highlighting"""
    languages = ('python','json','yaml','yml','rust','bash','zrsh','zsh','javascript','mysql','psql','sql','R','julia','dart','go')
    language = 'python' if language not in languages else language
    syntax = Syntax(text, language, theme=theme, line_numbers=False)
    console.print(syntax)


def print_text(text, role, show_role=True):
    """Print text with role-specific coloring"""
    color_dict = {'user':'yellow', 'assistant': 'green', 'system': 'magenta', 'error': 'red', 'menu': 'green', 'info': 'orange'}
    if show_role:
        regular_text = Text(f"\n{role.title()}: {text}")
        regular_text.stylize(f"bold italic {color_dict[role]}", 0, len(role)+1)
        regular_text.stylize(f"bold {color_dict[role]}", len(role)+1)
    else:
        regular_text = Text(text)
        regular_text.stylize(f"bold {color_dict[role]}")

    console.print(regular_text)


def process_text(messages, key, model='gpt-4', seed=None, verbose=False):
    """Call the openAI api using the given messages and return the response"""
    client = OpenAI(api_key=key)

    if seed is not None:
        messages.append({"role": "assistant", "content": seed})
    
    try:
        response = client.chat.completions.create(
            model=model,
            messages=messages
        )
        if verbose:
            print_text(response['usage']['total_tokens'], 'info', show_role=True)

        return response
    
    except Exception as e:
        wait_time = 10
        rich_print('error', f'{e}, Retrying in {wait_time}')
        time.sleep(wait_time)
        return process_text(messages, key, model, seed, verbose)