from utils import define_conversations, rich_print, process_text, print_title
from typing import List, Dict


class Message:                                                                                                                             
    def __init__(self, role:str, content:str):
        self.role = role
        self.content = content
        self.words = content.split()

    def shorten(self, cap):
        while len(self.content) > cap:
            self.words.pop(0)
        self.content = ' '.join(self.words)

    def print(self):
        rich_print(self.role, self.content)

    def jsonify(self):
        return {"role": self.role, "content": self.content}


class Conversation:
    def __init__(self, name:str, messages: List[Dict[str, str]]):
        self.name = name
        self.messages = [Message(message['role'], message['content']) for message in messages]
        self.db_document = define_conversations().find({'conversation_name': self.name})[0]

    def shorten(self, cap:int):
        total_len = 0
        for message in self.messages:
            total_len += len(message.content)
        while total_len > cap:
            if len(self.messages[0].content) == 0:
                self.messages.pop(0)
            else:
                self.messages[0].shorten(cap)
    
    def print(self):
        print_title(self.name)
        for message in self.messages:
            message.print()

    def jsonify_messages(self):
        return [message.jsonify() for message in self.messages]

    def pull_changes(self):
        self.db_document = define_conversations().find({'conversation_name': self.name})[0]

    def push_changes(self):
        conversations = define_conversations()
        filter = {'conversation_name': self.name}
        conversations.update_one(filter, { "$set": { "messages": self.jsonify_messages() } })
        self.pull_changes()

    def proceed(self, key:str, model:str='gpt-4'):
        """Continue an existing conversation"""
        running = True
        while running:

            user_input = input("Your message ('exit' to quit or 'file' to read message from message.txt): ")
            if user_input.lower().strip() == 'exit':
                running = False
                break

            if user_input == 'file':
                try:                                                                                                                                   
                    with open('message.txt', 'r') as file:                                                                                               
                        user_input = str(file.read())                                                                                                    
                except IOError:  
                    rich_print('error', 'Could not read file: message.txt')                                                                                                                      

            # Send message
            message = Message('user', user_input)
            message.print()
            self.messages.append(message)
            response = process_text(self.jsonify_messages(), key, model, seed=None, verbose=False)

            # Parse Response
            response_message = Message('assistant', response.choices[0].message.content)
            self.messages.append(response_message)
            response_message.print()
            
            # Update the database record for the conversation
            self.push_changes()
        
        return